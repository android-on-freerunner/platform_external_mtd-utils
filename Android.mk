# Copyright 2005 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(addprefix mtd-utils/, \
	crc32.c compr_rtime.c mkfs.jffs2.c compr_zlib.c compr_lzo.c \
        compr.c rbtree.c)

LOCAL_CFLAGS =   -O2 -Wall -DWITHOUT_XATTR
LOCAL_CFLAGS+=   -Wextra -Wwrite-strings -Wno-sign-compare
LOCAL_CFLAGS+=   -D_FILE_OFFSET_BITS=64
#LOCAL_CFLAGS+=   -Wshadow -Wpointer-arith -Wwrite-strings
#LOCAL_CFLAGS+=   -Wredundant-decls -Wnested-externs -Winline
#LOCAL_CFLAGS+=   -Wstrict-prototypes -Wmissing-declarations -Wmissing-prototypes

LOCAL_STATIC_LIBRARIES := liblzo2 libz

LOCAL_C_INCLUDES += $(LOCAL_PATH)/mtd-utils/include

LOCAL_MODULE := mkfs.jffs2

include $(BUILD_HOST_EXECUTABLE)

$(call dist-for-goals,user userdebug droid,$(LOCAL_BUILT_MODULE))
